﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticles : MonoBehaviour
{
    public bool reverseDir;

    void Start()
    {
        StartCoroutine(destroyProjectile());
    }

    void Update()
    {
        
    }

    IEnumerator destroyProjectile()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
