﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using DG.Tweening;

public class WhiteTransition : MonoBehaviour
{
    public GameObject player;
    public GameObject mainCamera;
    public GameObject playerHB;
    public GameObject bossHB;

    private NavMeshAgent agentPlayer;

    void OnEnable()
    {
        Color c = GetComponent<Image>().color;
        c.a = 0;
        GetComponent<Image>().color = c;
        StartTransition();
    }

    void Start()
    {
        //playerHB = GameObject.Find("PlayerHealthBar");
        //bossHB = GameObject.Find("BossHealthBar");
        player = GameObject.Find("Player");
        mainCamera = GameObject.Find("MainCamera");
        mainCamera.GetComponent<CameraController>().inCutscene = true;
        agentPlayer = player.GetComponent<NavMeshAgent>();
        agentPlayer.enabled = !agentPlayer.enabled;
    }

    void Update()
    {
        
    }

    void StartTransition()
    {
        GetComponent<Image>().DOFade(1, 1f).SetEase(Ease.OutCubic).OnComplete(teleport);
    }

    void teleport()
    {
        playerHB.SetActive(false);
        bossHB.SetActive(false);
        player.transform.position = new Vector3(140, 1, 420.9f);
        mainCamera.transform.position = new Vector3(149.3f, 8.1f, 418.6f);     
        mainCamera.transform.rotation = Quaternion.Euler(30, -50, 0);
        agentPlayer.enabled = !agentPlayer.enabled;
        FinishTransition();
    }

    void FinishTransition()
    {
        GetComponent<Image>().DOFade(0, 1f).SetEase(Ease.OutCubic).OnComplete(DisableTransitionObj);
    }

    void DisableTransitionObj()
    {
        gameObject.SetActive(false);
    }
}
