﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class QuitGame : MonoBehaviour
{
    Color color;

    public float duration = 1;

    void OnEnable()
    {
        color = GetComponent<Image>().color;
        color.a = 0;
        GetComponent<Image>().color = color;
        startTransition();
    }

    void Start()
    {
        
    }

    void startTransition()
    {
        GetComponent<Image>().DOFade(1, duration).SetEase(Ease.OutCubic).OnComplete(quitGame);
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
