﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;


public class LoseMenu : MonoBehaviour
{
    public MeshRenderer[] eyes;
    public GameObject nextSceneFadeOff;
    public bool buttonPressed;

    void Start()
    {
        buttonPressed = false;
        for (int i = 0; i < eyes.Length; i++)
        {
            eyes[i].material.SetColor("_EmissionColor", Color.red * 0);
        }
        StartCoroutine(waitToTurnlightsOn());
        nextSceneFadeOff.GetComponent<FadeOff>().nextScene = StorageGlobalVar.lastGameplayScene;
    }


    public void TryAgain()
    {       
        if (buttonPressed == false)
        {
            buttonPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            nextSceneFadeOff.SetActive(true);
        }
    }
    public void QuitGame()
    {
        FindObjectOfType<AudioManager>().Play("ButtonSound");
        Application.Quit();
    }


    

    public void turnOnEyes()
    {
        for (int i = 0; i < eyes.Length; i++)
        {
            eyes[i].material.DOColor(Color.red * 2, "_EmissionColor", 1);
        }
    }

    IEnumerator waitToTurnlightsOn()
    {
        yield return new WaitForSeconds(1);
        turnOnEyes();
    }
}
