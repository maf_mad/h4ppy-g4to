﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject nextLevelFadeOff;
    public GameObject exitGameFadeOff;
    public GameObject player;

    void OnEnable()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(GameIsPaused)
            {
                Resume();
            }
            else
            {
                FindObjectOfType<AudioManager>().Play("Pause");
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
        Time.timeScale = 1f;
        player.GetComponent<PlayerController>().enableControls();
    }

    public void Pause()
    {
        player.GetComponent<PlayerController>().disableControls();
        pauseMenuUI.SetActive(true);
        GameIsPaused = true;
        Time.timeScale = 0f;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        nextLevelFadeOff.GetComponent<FadeOff>().nextScene = "Menu";
        nextLevelFadeOff.SetActive(true);
        //SceneManager.LoadScene("Menu");
    }

    public void QuitGame()
    {
        Time.timeScale = 1f;
        exitGameFadeOff.SetActive(true);
    }
}
