﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeIn : MonoBehaviour
{
    Color color;

    void Start()
    {
        StartCoroutine(loadingTime());
        color = GetComponent<Image>().color;
        color.a = 1;
        GetComponent<Image>().color = color;
    }

    void Update()
    {
        
    }

    IEnumerator loadingTime()
    {
        yield return new WaitForSeconds(3);
        startTransition();
    }

    void startTransition()
    {
        GetComponent<Image>().DOFade(0, 1).SetEase(Ease.OutCubic).OnComplete(disableObj);
    }

    void disableObj()
    {
        gameObject.SetActive(false);
    }
}
