﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    LevelLoader lvlLoader;
    public Transform cam;

    public Renderer[] blueParts;
    public Light playerLight;
    public Renderer defaultMatColor;
    public bool buttonPressed;
    public GameObject nextSceneFadeOff;
    public GameObject exitGameFadeOff;

    void Awake()
    {
        defaultMatColor = blueParts[1];
    }

    void Start()
    {
        buttonPressed = false;
        setLightsRed();
        lvlLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();
        playerLight.color = Color.red*2;
    }
    public void PlayGame()
    {
        if (buttonPressed==false)
        {
            buttonPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            moveCam();
            StartCoroutine(waitToChangeScene());
            StartCoroutine(waitToChangeColor());
        }      
    }

    public void QuitGame()
    {
        if (buttonPressed == false)
        {
            buttonPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            exitGameFadeOff.SetActive(true);
        }       
    }

    void moveCam()
    {
        cam.DOMove(new Vector3 (11.13769f,1.277535f,-12.08694f), 2).SetEase(Ease.InOutQuad);
        cam.DORotate(new Vector3(1.65f, 94.538f, 0), 2).SetEase(Ease.InOutQuad);
    }

    void setLightsRed()
    {
        for (int i = 0; i < blueParts.Length; i++)
        {
            blueParts[i].material.SetColor("_EmissionColor", Color.red * 2);
        }       
    }

    void animLightsPlayer()
    {
        for (int i = 0; i < blueParts.Length; i++)
        {
            blueParts[i].material.DOColor(defaultMatColor.material.color * 3, "_EmissionColor", 1);
        }

        playerLight.DOColor(defaultMatColor.material.color * 3, 1);
    }

    IEnumerator waitToChangeColor()
    {
        yield return new WaitForSeconds(2.5f);
        animLightsPlayer();
    }

    IEnumerator waitToChangeScene()
    {
        yield return new WaitForSeconds(4f);
        //SceneManager.LoadScene("Tutorial");
        nextSceneFadeOff.SetActive(true);
    }


}
