﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public bool buttonPressed;
    public GameObject nextSceneFadeOff;

    void Start()
    {
        nextSceneFadeOff.GetComponent<FadeOff>().nextScene = "Menu";
    }

    public void goToMenu()
    {
        if (buttonPressed == false)
        {
            buttonPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            nextSceneFadeOff.SetActive(true);
        }
    }

    public void QuitGame()
    {
        if (buttonPressed == false)
        {
            buttonPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonSound");
            Application.Quit();
        }
    }
}
