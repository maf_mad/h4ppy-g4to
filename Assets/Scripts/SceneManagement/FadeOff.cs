﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class FadeOff : MonoBehaviour
{
    Color color;

    public float duration = 1;
    public string nextScene;

    void OnEnable()
    {
        color = GetComponent<Image>().color;
        color.a = 0;
        GetComponent<Image>().color = color;
    }

    void Start()
    {
        startTransition();
    }

    void startTransition()
    {
        GetComponent<Image>().DOFade(1, duration).SetEase(Ease.OutCubic).OnComplete(changeScene);
    }

    public void changeScene()
    {
        SceneManager.LoadScene(nextScene);
    }
}
