﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MissionUI : MonoBehaviour
{
    public Transform showPos;
    public Transform originalPos;
    public bool showMissionAtStart = true;
    public float timeShowing = 3;

    void Start()
    {
        if (showMissionAtStart==true)
        {
            StartCoroutine(waitToShowMission());
            //StartCoroutine(waitToHideMission());
        }
        
    }

    void Update()
    {
        
    }

    IEnumerator waitToShowMission()
    {
        yield return new WaitForSeconds(4);
        showAndHideMission();
    }

    public void showAndHideMission()
    {
        showMission();
        StartCoroutine(waitToHideMission());
    }

    public void showMission()
    {
        GetComponent<RectTransform>().DOMove(showPos.position,1);
    }

    IEnumerator waitToHideMission()
    {
        yield return new WaitForSeconds(timeShowing);
        hideMission();
    }

    void hideMission()
    {
        GetComponent<RectTransform>().DOMove(originalPos.position, 1);
    }
}
