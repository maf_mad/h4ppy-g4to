﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationsPlayer : MonoBehaviour
{
    public Renderer[] blueParts;
    public Renderer defaultMatColor;
    public Light playerLight;

    void Start()
    {
        defaultMatColor = blueParts[1];
    }

    void Update()
    {
        
    }

    public void goingToExplode()
    {
        for (int i = 0; i < blueParts.Length; i++)
        {
            blueParts[i].material.DOColor(defaultMatColor.material.color * 5, "_EmissionColor", 1.5f);
        }

        playerLight.DOColor(defaultMatColor.material.color * 5, 1.5f);

    }
}
