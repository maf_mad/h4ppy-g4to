﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTakeDamage : MonoBehaviour
{
    public PlayerLife playerHealth;
    public PlayerController playerController;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnemyExplosion") && playerController.invincible == false)
        {
            playerHealth.ReceiveDmg(20);
        }
        if (other.gameObject.CompareTag("EnemyProjectile") && playerController.invincible == false)
        {
            playerHealth.ReceiveDmg(20);
            Destroy(other.gameObject);
        }
    }
}
