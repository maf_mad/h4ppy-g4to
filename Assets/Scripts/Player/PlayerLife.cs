﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{

    public int maxHealth = 200;
    public int currentHealth;
    public bool death = false;

    public GameObject healthBar;
    public PlayerController playerController;
    public ShakeAnimation shakeAnimation;
    public GameObject model;
    public ParticleSystem deathParticles;
    public CapsuleCollider bodyCol;
    public AnimController animController;
    public AnimationsPlayer animationsPlayer;

    public GameObject nextSceneFadeOff;

    void Start()
    {
        healthBar = GameObject.Find("PlayerHealthBar");
        currentHealth = maxHealth;
        healthBar.GetComponent<HealthBar>().SetMaxHealth(maxHealth);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.G))
        {
            ReceiveDmg(20);
        }
        if (currentHealth <= 0 && death == false)
        {
            animController.disableAnimator();
            death = true;
            //SceneManager.LoadScene("Lose");
            playerController.disableControls();
            shakeAnimation.Shake();
            StartCoroutine(waitToExplode());
            animationsPlayer.goingToExplode();
        }
    }

    public void ReceiveDmg(int damage)
    {
        currentHealth -= damage;
        FindObjectOfType<AudioManager>().Play("HitToPlayer");
        healthBar.GetComponent<HealthBar>().SetHealth(currentHealth);
    }

    IEnumerator waitToExplode()
    {
        yield return new WaitForSeconds(1.5f);
        deathParticles.Play();
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
        model.SetActive(false);
        bodyCol.enabled = false;        
        nextSceneFadeOff.GetComponent<FadeOff>().nextScene = "Lose";
        nextSceneFadeOff.SetActive(true);
    }
}
