﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public Animator anim;

    void Start()
    {
        
    }

    void Update()
    {

    }

    public void isMovingTrue()
    {
        anim.SetBool("isMoving", true);
    }

    public void isMovingFalse()
    {
        anim.SetBool("isMoving",false);
    }

    public void isShootingTrue()
    {
        anim.SetBool("isShooting", true);
    }

    public void isShootingFalse()
    {
        anim.SetBool("isShooting", false);
    }

    public void isAttackingTrue()
    {
        anim.SetBool("isAttacking", true);
    }

    public void isAttackingFalse()
    {
        anim.SetBool("isAttacking", false);
    }

    public void isDashingTrue()
    {
        anim.SetBool("isDashing", true);
    }

    public void isDashingFalse()
    {
        anim.SetBool("isDashing", false);
    }

    public void Fire()
    {
        anim.Play("A_Shoot");
    }

    public void Attack()
    {
        anim.Play("New_A_Melee");
    }

    public void Death()
    {

    }

    public void disableAnimator()
    {
        anim.enabled = false;
    }
}
