﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public GameObject cam;
    public NavMeshAgent agent;
    public Shooting shoting;
    public AnimController animController;
    //public PlayerLife playerLife;
    public ParticleSystem tackleParticles;
    public GameObject tackleForceField;
    public bool isControllable = false;
    public bool isMoving;
    public bool canDash = false;
    public bool isDashing = false;
    public bool startActive = true;
    public bool invincible = false;
    public bool canChargeSp = false;
    public float timeToEnableControls = 4;

    public float maxSp = 100;
    public float currentSp;
    public GameObject spBar;


    RaycastHit hit;

    void Start()
    {
        spBar = GameObject.Find("PlayerSpecialBar");
        spBar.GetComponent<SpecialBar>().SetMaxSpecial(maxSp);

        cam = GameObject.Find("MainCamera");
        currentSp = 0;
        if (startActive == true)
        {
            StartCoroutine(waitToEnableControls());
        }
    }

    void Update()
    {       
        if (isControllable==true)
        {
            Click();
        }
        
        checkIfIsMoving();
        chargeSPBar();
        Special();

        if (currentSp>=maxSp)
        {
            canDash = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            canDash = true;
        }

        spBar.GetComponent<SpecialBar>().SetSpecial(currentSp);
    }

    public void disableControls()
    {
        isControllable = false;
        canChargeSp = false;
    }

    public void enableControls()
    {
        isControllable = true;
        canChargeSp = true;
    }

    void Click()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag("EnemyClickCol"))
                {
                    Attack();
                }
                if (hit.transform.CompareTag("Floor"))
                {
                    Move();
                }
            }
        }

        if (Input.GetMouseButtonDown(2) || Input.GetKeyDown(KeyCode.Space))
        {
            if (canDash == true)
            {
                FindObjectOfType<AudioManager>().Play("PlayerDash");
                starSpecial();
            }
        }       
    }

    void Move()
    {
        agent.SetDestination(hit.point);
    }

    void Attack()
    {
        shoting.lockedEnemy = hit.transform.gameObject;
    }

    void starSpecial()
    {
        Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Floor"))
            {
                currentSp = 0;
                canDash = false;
                tackleForceField.GetComponent<Transform>().DOScale(2, 0.3f).SetEase(Ease.OutCubic);
                tackleParticles.Play();
                isDashing = true;
                agent.speed = 30;
                agent.acceleration = 999;
                invincible = true;
                StartCoroutine(waitToStopSpecial());
            }
        }
    }

    void Special()
    {
        if (isDashing == true) {
            Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag("Floor"))
                {
                    agent.SetDestination(hit.point);
                }
            }
        }
    }

    public void stopSpecial()
    {
        tackleForceField.GetComponent<Transform>().DOScale(0, 0.3f).SetEase(Ease.OutCubic);
        tackleParticles.Stop();
        agent.speed = 6;
        agent.acceleration = 50;
        isDashing = false;
        invincible = false;
    }

    void checkIfIsMoving()
    {
        if (agent.hasPath == true)
        {
            isMoving = true;
            animController.isMovingTrue();
        }
        else
        {
            isMoving = false;
            animController.isMovingFalse();
        }
    }

    void chargeSPBar()
    {
        if (currentSp < maxSp && canChargeSp)
        {
            currentSp = currentSp + 1 * Time.deltaTime * 2;
        }
    }

    IEnumerator waitToEnableControls()
    {
        yield return new WaitForSeconds(timeToEnableControls);
        enableControls();
        canChargeSp = true;
    }

    IEnumerator waitToStopSpecial()
    {
        yield return new WaitForSeconds(5);
        stopSpecial();
    }
}
