﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    //public Shooting shooting;
    float projectileSpeed = 50;
    public Rigidbody rbBullet;


    void Start()
    {
        rbBullet.velocity = transform.forward * projectileSpeed;
        StartCoroutine(destroyProjectile());

    }

    void Update()
    {

    }

    IEnumerator destroyProjectile()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }
}
