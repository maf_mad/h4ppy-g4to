﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject lockedEnemy;

    public GameObject projectilePrefab;
    public Transform gunTransform;
    public float rechargeTime;
    public bool Charged = true;
    public PlayerLife playerLife;

    public AnimController animController;

    void Start()
    {
        
    }


    void Update()
    {
        lookAtEnemy();

        if (lockedEnemy != null && Charged == true && playerLife.death==false)
        {           
            Fire();
        }

        if (lockedEnemy == null)
        {
            animController.isShootingFalse();

        }
    }

    void Fire()
    {
        Instantiate(projectilePrefab, new Vector3(gunTransform.position.x, gunTransform.position.y, gunTransform.position.z), gunTransform.rotation);
        animController.isShootingTrue();
        animController.Fire();
        Charged = false;
        StartCoroutine(Recharge());
        FindObjectOfType<AudioManager>().Play("PlayerShoot");
    }

    void lookAtEnemy()
    {
        if (lockedEnemy != null)
        {
            transform.LookAt(lockedEnemy.transform);
        }
    }

    IEnumerator Recharge()
    {
        yield return new WaitForSeconds(rechargeTime);
        Charged = true;
    }
}
