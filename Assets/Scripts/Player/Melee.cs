﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{
    public GameObject bladeHitbox;
    public bool isAttacking=false;
    public LookAtAttackingDirection lookAtAttackingDirection;
    public PlayerController controllerPlayer;

    public AnimController animController;

    void Update()
    {
        if (controllerPlayer.isControllable==true)
        {
            startAttack();
        }
    }

    void startAttack()
    {
        if (Input.GetMouseButtonDown(0)&&isAttacking==false)
        {
            isAttacking = true;
            lookAtAttackingDirection.Click();
            StartCoroutine(inputLag());
            animController.Attack();
            FindObjectOfType<AudioManager>().Play("PlayerSlash");
        }
    }

    IEnumerator inputLag()
    {
        yield return new WaitForSeconds(0.1f);
        Slash();
    }

    void Slash()
    {
        bladeHitbox.SetActive(true);
        StartCoroutine(hitboxDuration());
    }

    IEnumerator hitboxDuration()
    {
        yield return new WaitForSeconds(0.2f);
        disableHitbox();
    }

    void disableHitbox()
    {
        bladeHitbox.SetActive(false);
        StartCoroutine(endLag());
    }

    IEnumerator endLag()
    {
        yield return new WaitForSeconds(0.2f);
        isAttacking = false;
    }
}
