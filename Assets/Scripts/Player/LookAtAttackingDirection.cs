﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtAttackingDirection : MonoBehaviour
{
    public GameObject cam;
    RaycastHit hit;
    public GameObject lookDirObj;

    void Update()
    {
        //Click();
    }

    void Start()
    {
        cam = GameObject.Find("MainCamera");
    }

    public void Click()
    {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                setDirObj();
                lookAtObj();
            }
        }
    }

    void setDirObj()
    {
        lookDirObj.transform.position = hit.point;
    }

    void lookAtObj()
    {
        gameObject.transform.LookAt(new Vector3(lookDirObj.transform.position.x, 0f, lookDirObj.transform.position.z));
    }
}
