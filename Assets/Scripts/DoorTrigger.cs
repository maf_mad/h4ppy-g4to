﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    Doors doorsComp;
    public GameObject doors;

    void Start()
    {
        doorsComp = doors.GetComponent<Doors>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            doorsComp.SlideDoors(true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            doorsComp.SlideDoors(false);
        }
    }
}
