﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreBattleSequence : MonoBehaviour
{
    public ControllerBoss controllerBoss;
    public AnimationsBoss animationBoss;
    public Phase1Sequence phase1Sequence;
    public MissionUI missionUI;
    public PlayerController playerController;

    public GameObject healthBar;

    void Start()
    {
        
    }

    void Update()
    {

    }

    public void startSequence()
    {
        StartCoroutine(turnOnEyes());
        StartCoroutine(turnOnSpikes());
        StartCoroutine(turnOnLightsAndMusic());
        StartCoroutine(waitToActivateForceField());
        StartCoroutine(waitToActivateHealthBar());
        StartCoroutine(waitToStartPhase1());
        StartCoroutine(waitToShowMission());
    }

    IEnumerator turnOnEyes()
    {
        yield return new WaitForSeconds(1);
        animationBoss.turnOnEyes();
    }

    IEnumerator turnOnSpikes()
    {
        yield return new WaitForSeconds(2);
        animationBoss.turnOnSpikes();
    }

    IEnumerator turnOnLightsAndMusic()
    {
        yield return new WaitForSeconds(3);
        animationBoss.turnOnRoomTopLight();
    }

    IEnumerator waitToActivateForceField()
    {
        yield return new WaitForSeconds(3);
        animationBoss.enableForceField();
    }

    IEnumerator waitToShowMission()
    {
        yield return new WaitForSeconds(3.5f);
        missionUI.showAndHideMission();
    }

    IEnumerator waitToActivateHealthBar()
    {
        yield return new WaitForSeconds(3.5f);
        healthBar.SetActive(true);
    }

    IEnumerator waitToStartPhase1()
    {
        yield return new WaitForSeconds(6.5f);
        playerController.enableControls();
        phase1Sequence.startSequence();
    }
}
