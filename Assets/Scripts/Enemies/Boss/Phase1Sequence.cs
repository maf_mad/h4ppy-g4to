﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase1Sequence : MonoBehaviour
{
    public ControllerBoss controllerBoss;
    public AnimationsBoss animationsBoss;
    public ShootingBoss shootingBoss;
    public SpawnEnemiesBoss spawnEnemiesBoss;
    public Phase2Sequence phase2Sequence;
    public Phase3Sequence phase3Sequence;

    void Start()
    {
        
    }

    void Update()
    {

    }

    public void startSequence()
    {
        if (controllerBoss.phase==1)
        {
            spawnEnemA();
            StartCoroutine(waitToSpawnEnem1());
            StartCoroutine(waitToSpawnEnem2());
            StartCoroutine(waitToDisableShield());
            StartCoroutine(waitToEnableShield());
            StartCoroutine(waitToStartAgain());
        }
        else if (controllerBoss.phase==2)
        {
            phase2Sequence.startSequence();
        }
        else if (controllerBoss.phase ==3)
        {
            phase3Sequence.startSequence();
        }
    }

    void spawnEnemA()
    {
        spawnEnemiesBoss.spawnEnemyA();
    }

    IEnumerator waitToSpawnEnem1()
    {
        yield return new WaitForSeconds(4);
        spawnEnemA();
    }

    IEnumerator waitToSpawnEnem2()
    {
        yield return new WaitForSeconds(8);
        spawnEnemA();
    }

    IEnumerator waitToDisableShield()
    {
        yield return new WaitForSeconds(12);
        FindObjectOfType<AudioManager>().Play("BossNoShield");
        animationsBoss.disableForceField();
    }

    IEnumerator waitToEnableShield()
    {
        yield return new WaitForSeconds(28);
        animationsBoss.enableForceField();
        controllerBoss.pushPlayer();
        FindObjectOfType<AudioManager>().Play("BossShield");
    }

    IEnumerator waitToStartAgain()
    {
        yield return new WaitForSeconds(30);
        startSequence();
    }
}
