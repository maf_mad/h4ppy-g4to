﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class DeathSequence : MonoBehaviour
{
    //public ControllerBoss controllerBoss;
    public AnimationsBoss animationsBoss;
    public AnimControllerBoss animControllerBoss;
    public GameObject playerGO;
    public Transform postBattlePoint;
    public GameObject exitPoint;

    public GameObject finishSequenceObj;
    public GameObject nextSceneFadeOff;
    public bool isDead = false;

    void Start()
    {
        playerGO = GameObject.Find("Player");
    }

    void Update()
    {
        
    }

    public void startSequence()
    {
        playerGO.GetComponent<PlayerController>().disableControls();
        finishSequenceObj.SetActive(true);
        animControllerBoss.Death();
        StartCoroutine(waitToExitRoom());
        StartCoroutine(waitToChangeScene());
    }

    IEnumerator waitToExitRoom()
    {
        yield return new WaitForSeconds(5);
        goToExitPoint();
    }

    IEnumerator waitToChangeScene()
    {
        yield return new WaitForSeconds(8);
        nextSceneFadeOff.SetActive(true);
    }

    public void goToExitPoint()
    {
        playerGO.GetComponent<NavMeshAgent>().SetDestination(exitPoint.transform.position);
    }

    /*
    IEnumerator waitToRotate()
    {
        yield return new WaitForSeconds(3);
        rotate();
    }

    

    void rotate()
    {
        playerGO.GetComponent<Transform>().DORotate(new Vector3(0, 0, 0), 1);
    }

    void exitRoom()
    {
        playerGO.GetComponent<NavMeshAgent>().SetDestination(exitPoint.transform.position);
    }*/
}
