﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemiesBoss : MonoBehaviour
{
    public GameObject spawnPoint1;
    public GameObject spawnPoint2;
    public GameObject spawnPoint3;
    public GameObject spawnPoint4;
    public GameObject spawnPoint5;
    public GameObject spawnPoint6;

    public GameObject prefabEnemyA;
    public GameObject prefabEnemyB;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void spawnEnemyA()
    {
        Instantiate(prefabEnemyA, spawnPoint1.transform.position, spawnPoint1.transform.rotation);
        Instantiate(prefabEnemyA, spawnPoint2.transform.position, spawnPoint2.transform.rotation);
        Instantiate(prefabEnemyA, spawnPoint3.transform.position, spawnPoint3.transform.rotation);
        Instantiate(prefabEnemyA, spawnPoint4.transform.position, spawnPoint4.transform.rotation);
        Instantiate(prefabEnemyA, spawnPoint5.transform.position, spawnPoint5.transform.rotation);
        Instantiate(prefabEnemyA, spawnPoint6.transform.position, spawnPoint6.transform.rotation);
    }

    public void spawnEnemyB()
    {
        Instantiate(prefabEnemyB, spawnPoint1.transform.position, spawnPoint1.transform.rotation);
        Instantiate(prefabEnemyB, spawnPoint2.transform.position, spawnPoint2.transform.rotation);
        Instantiate(prefabEnemyB, spawnPoint3.transform.position, spawnPoint3.transform.rotation);
        Instantiate(prefabEnemyB, spawnPoint4.transform.position, spawnPoint4.transform.rotation);
        Instantiate(prefabEnemyB, spawnPoint5.transform.position, spawnPoint5.transform.rotation);
        Instantiate(prefabEnemyB, spawnPoint6.transform.position, spawnPoint6.transform.rotation);
    }
}
