﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerBoss : MonoBehaviour
{
    public Animator anim;
    public Animator animcs;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void activateCanon()
    {
        anim.SetBool("ActivateCanon", true);
    }

    public void Fire()
    {
        anim.Play("1Shoot");
    }

    public void Death()
    {
        animcs.SetBool("IsDead", true);
    }
}
