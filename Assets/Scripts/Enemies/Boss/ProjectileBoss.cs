﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ProjectileBoss : MonoBehaviour
{
    float speed = 15;
    public Rigidbody projectileRigidbody;
    public GameObject collisionParticles;

    void Start()
    {
        grow();
        projectileRigidbody.velocity = transform.forward * speed;
        StartCoroutine(destroyProjectile());
    }

    IEnumerator destroyProjectile()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        /*if (other.CompareTag("Wall"))
        {
            Instantiate(collisionParticles, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), gameObject.transform.rotation);

            Destroy(gameObject);
        }*/
        if (other.CompareTag("Player"))
        {
            Instantiate(collisionParticles, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), gameObject.transform.rotation);
        }
        else if (other.CompareTag("Floor"))
        {
            Instantiate(collisionParticles, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), gameObject.transform.rotation);
            Destroy(gameObject);
        }
    }

    void grow()
    {
        GetComponent<Transform>().DOScale(0.6f, 0.3f).SetEase(Ease.OutCubic);
    }
}
