﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase2Sequence : MonoBehaviour
{
    public ControllerBoss controllerBoss;
    public AnimationsBoss animationsBoss;
    public ShootingBoss shootingBoss;
    public SpawnEnemiesBoss spawnEnemiesBoss;
    public Phase3Sequence phase3Sequence;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void startSequence()
    {
        if (controllerBoss.phase == 2)
        {
            spawnEnemA();
            StartCoroutine(waitToSpawnEnemB());
            StartCoroutine(waitToSpawnEnemA());
            StartCoroutine(waitToDisableShield());
            StartCoroutine(waitToEnableShield());
            StartCoroutine(waitToStartAgain());
        }
        else if (controllerBoss.phase == 3)
        {
            phase3Sequence.startSequence();
        }
    }

    void spawnEnemA()
    {
        spawnEnemiesBoss.spawnEnemyA();
    }

    IEnumerator waitToSpawnEnemB()
    {
        yield return new WaitForSeconds(4);
        spawnEnemiesBoss.spawnEnemyB();
    }

    IEnumerator waitToSpawnEnemA()
    {
        yield return new WaitForSeconds(12);
        spawnEnemiesBoss.spawnEnemyA();
    }

    IEnumerator waitToDisableShield()
    {
        yield return new WaitForSeconds(20);
        animationsBoss.disableForceField();
        FindObjectOfType<AudioManager>().Play("BossNoShield");
    }

    IEnumerator waitToEnableShield()
    {
        yield return new WaitForSeconds(40);
        animationsBoss.enableForceField();
        controllerBoss.pushPlayer();
        FindObjectOfType<AudioManager>().Play("BossShield");
    }

    IEnumerator waitToStartAgain()
    {
        yield return new WaitForSeconds(42);
        startSequence();
    }
}
