﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageBoss : MonoBehaviour
{
    public ControllerBoss controllerBoss;
    public int damageAmountByBlade = 5;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BladeHitbox"))
        {
            takeDamageByBlade();
        }
    }

    void takeDamageByBlade()
    {
        controllerBoss.currentHealth -= damageAmountByBlade;
        controllerBoss.healthBar.SetHealth(controllerBoss.currentHealth);
    }
}
