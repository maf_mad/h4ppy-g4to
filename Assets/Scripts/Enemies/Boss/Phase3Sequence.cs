﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase3Sequence : MonoBehaviour
{
    public ControllerBoss controllerBoss;
    public AnimationsBoss animationsBoss;
    public ShootingBoss shootingBoss;
    public SpawnEnemiesBoss spawnEnemiesBoss;
    public DeathSequence deathSequence;
    public AnimControllerBoss animControllerBoss;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void startSequence()
    {


        
        animControllerBoss.activateCanon();
        StartCoroutine(waitToStartShooting());
        StartCoroutine(waitToSpawnEnemA());
        StartCoroutine(waitToSpawnEnemB());
        StartCoroutine(waitToStopShooting());
        StartCoroutine(waitToDisableShield());
        StartCoroutine(waitToEnableShield());
        StartCoroutine(waitToStartAgain());
    }

    IEnumerator waitToStartShooting()
    {
        yield return new WaitForSeconds(3);
        shootingBoss.startShooting();
    }

    IEnumerator waitToStopShooting()
    {
        yield return new WaitForSeconds(18);
        shootingBoss.stopShooting();
    }

    IEnumerator waitToSpawnEnemA()
    {
        yield return new WaitForSeconds(4);
        spawnEnemiesBoss.spawnEnemyA();
    }

    IEnumerator waitToSpawnEnemB()
    {
        yield return new WaitForSeconds(12);
        spawnEnemiesBoss.spawnEnemyB();
    }

    IEnumerator waitToDisableShield()
    {
        yield return new WaitForSeconds(20);
        animationsBoss.disableForceField();
        FindObjectOfType<AudioManager>().Play("BossNoShield");
    }

    IEnumerator waitToEnableShield()
    {
        yield return new WaitForSeconds(40);
        shieldnPush();
        FindObjectOfType<AudioManager>().Play("BossShield");
    }

    void shieldnPush()
    {
        if (controllerBoss.phase==3)
        {
            animationsBoss.enableForceField();
            controllerBoss.pushPlayer();
        }
    }

    IEnumerator waitToStartAgain()
    {
        yield return new WaitForSeconds(42);
        startSequence();
    }
}
