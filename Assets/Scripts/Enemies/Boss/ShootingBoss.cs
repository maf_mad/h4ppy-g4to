﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingBoss : MonoBehaviour
{
    public GameObject projectilePrefab;
    public GameObject projectileSpawnPoint;
    public GameObject player;

    public float rechargeTime;

    public ControllerBoss controllerBoss;
    public AnimControllerBoss animControllerBoss;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            startShooting();
        }

        projectileSpawnPoint.transform.LookAt(player.transform);
    }

    public void startShooting()
    {
        InvokeRepeating("Fire", 0, 0.25f);
    }

    public void stopShooting()
    {
        CancelInvoke("Fire");
    }

    public void Fire()
    {
        animControllerBoss.Fire();
        Instantiate(projectilePrefab, new Vector3(projectileSpawnPoint.transform.position.x, projectileSpawnPoint.transform.position.y, projectileSpawnPoint.transform.position.z), projectileSpawnPoint.transform.rotation);
    }

}
