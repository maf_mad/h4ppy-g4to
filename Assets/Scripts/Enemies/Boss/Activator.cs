﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Activator : MonoBehaviour
{
    public GameObject player;
    public ControllerBoss controllerBoss;
    public GameObject battlePoint;
    public Phase1Sequence phase1Sequence;
    public PreBattleSequence preBattleSequence;
    public AnimationsBoss animationsBoss;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player.GetComponent<PlayerController>().disableControls();
            player.GetComponent<PlayerController>().stopSpecial();
            movePlayerToBattlePoint();
            StartCoroutine(waitToStartPreSequence());
            StartCoroutine(waitToDestroyActivator());
        }
    }

    void startBoss()
    {
        controllerBoss.isActive = true;
    }

    void movePlayerToBattlePoint()
    {
        player.GetComponent<PlayerController>().agent.SetDestination(battlePoint.transform.position);
    }

    IEnumerator waitToStartPreSequence()
    {
        yield return new WaitForSeconds(2);
        preBattleSequence.startSequence();        
    }

    IEnumerator waitToDestroyActivator()
    {
        yield return new WaitForSeconds(5);
        animationsBoss.canRotate = true;
        //player.GetComponent<PlayerController>().enableControls();
        Destroy(gameObject);
    }
}
