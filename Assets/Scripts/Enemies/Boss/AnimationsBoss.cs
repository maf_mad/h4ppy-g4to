﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationsBoss : MonoBehaviour
{
    public GameObject bodyGO;
    public GameObject playerGO;
    public GameObject forceFieldGO;
    public GameObject roomTopLight;
    public GameObject bossCol;

    public MeshRenderer[] eyes;
    public MeshRenderer[] emissiveSpikes;

    public bool canRotate = false;

    public ControllerBoss controllerBoss;

    void Start()
    {
        playerGO = GameObject.Find("Player");

        for (int i = 0; i < eyes.Length; i++)
        {
            eyes[i].material.SetColor("_EmissionColor", Color.red * 0);
        }

        for (int i = 0; i < emissiveSpikes.Length; i++)
        {
            emissiveSpikes[i].material.SetColor("_EmissionColor", Color.red * 0);
        }
    }

    void Update()
    {
        bodyRotation();
    }

    void bodyRotation()
    {
        if (canRotate == true)
        {
            bodyGO.transform.LookAt(new Vector3(playerGO.transform.position.x, bodyGO.transform.position.y, playerGO.transform.position.z));
        }
    }

    public void turnOnEyes()
    {
        for (int i = 0; i < eyes.Length; i++)
        {
            eyes[i].material.DOColor(Color.red * 3, "_EmissionColor", 1);
        }
    }

    public void turnOnSpikes()
    {
        for (int i = 0; i < emissiveSpikes.Length; i++)
        {
            emissiveSpikes[i].material.DOColor(Color.red * 3, "_EmissionColor", 1);
        }
    }

    public void enableForceField()
    {
        forceFieldGO.SetActive(true);
        controllerBoss.forceFieldActive = true;
        bossCol.SetActive(false);
        forceFieldGO.GetComponent<Transform>().DOScale(9.9f, 1).SetEase(Ease.OutCubic);
    }

    public void disableForceField()
    {
        controllerBoss.forceFieldActive = false;
        bossCol.SetActive(true);
        blinkForceField();
        StartCoroutine(blinkingDuration());
    }


    void enabeDisableForceFiel()
    {
        if (forceFieldGO.activeSelf)
            forceFieldGO.SetActive(false);
        else
            forceFieldGO.SetActive(true);
    }

    void blinkForceField()
    {
        InvokeRepeating("enabeDisableForceFiel", 0, 0.06f);
    }

    IEnumerator blinkingDuration()
    {
        yield return new WaitForSeconds(0.5f);
        stopBlinkForceField();
    }

    void stopBlinkForceField()
    {
        CancelInvoke("enabeDisableForceFiel");
        forceFieldGO.transform.localScale = new Vector3 (0,0,0);
        forceFieldGO.SetActive(false);
    }


    public void turnOnRoomTopLight()
    {
        roomTopLight.GetComponent<Light>().DOIntensity(1.5f, 0.5f).SetEase(Ease.Flash);
    }

}
