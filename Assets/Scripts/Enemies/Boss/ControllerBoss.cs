﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControllerBoss : MonoBehaviour
{
    public bool isActive = false;
    public string signal = "none";
    public int phase = 0;
    public int initialHealth = 300;
    public int currentHealth;
    public bool dead = false;


    public GameObject body;

    public ShootingBoss shootingBoss;
    public AnimationsBoss animationBoss;
    public GameObject playerGO;
    public SpawnEnemiesBoss spawnEnemiesBoss;
    public DeathSequence deathSequence;

    public bool forceFieldActive;

    public HealthBar healthBar;
    public GameObject canvasBar;

    public Vector3 relativePos;
    public Vector3 newPlayerPos;

    public CapsuleCollider playerBodyCol;

    void Start()
    {
        playerGO = GameObject.Find("Player");
        healthBar.SetMaxHealth(initialHealth);
    }

    void Update()
    {
        getRelativePos();
        setNewPlayerPos();

        

        if (signal=="start")
        {
            signal = "none";
            animationBoss.canRotate = true;
            playerGO.GetComponent<PlayerController>().isControllable = true;
            FindObjectOfType<AudioManager>().Play("BossActivator");
        }

        /*if (currentHealth < initialHealth)
        {
            ShowHealthBar();
        }*/

        checkForHealth();
    }

    void checkForHealth()
    {
        if (currentHealth>=201)
        {
            phase = 1;
        }
        else if (currentHealth<=200 && currentHealth >= 101)
        {
            phase = 2;
        }
        else if (currentHealth <= 100 && currentHealth > 0)
        {
            phase = 3;
        }
        else if (currentHealth <= 0 && dead==false)
        {
            dead = true;
            FindObjectOfType<AudioManager>().Play("BossDeath");
            deathSequence.startSequence();
            playerBodyCol.enabled = false;
        }
    }

    void Fire()
    {
        shootingBoss.Fire();
    }

    void getRelativePos()
    {
        relativePos = playerGO.transform.position - transform.position;
    }

    void setNewPlayerPos()
    {
        newPlayerPos = relativePos + playerGO.transform.position;
    }

    public void pushPlayer()
    {
        playerGO.GetComponent<NavMeshAgent>().SetDestination(newPlayerPos);
    }

    void ShowHealthBar()
    {
        canvasBar.SetActive(true);
    }
}
