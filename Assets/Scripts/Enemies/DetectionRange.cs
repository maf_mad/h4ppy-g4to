﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Collections.Generic;

public class DetectionRange : MonoBehaviour
{
    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    public List<Transform> visibleTargets = new List<Transform>();

    public ControllerEnemA controllerEnemA;

    public ControllerEnemB controllerEnemB;

    void Start()
    {
        //Usa un coroutine para retrasaar un poco la ejecución del método(supongo para evitar un error)
        StartCoroutine("FindTargetsWithDelay", .2f);
    }

    IEnumerator FindTargetsWithDelay (float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visibleTargets.Add(target);
                    Actions();
                    
                }
            }
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal){
        if (!angleIsGlobal) {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    void Actions()
    {
        if (gameObject.CompareTag("FieldOfViewEnemB") && controllerEnemB.status == "patrolling")
        {
            controllerEnemB.detectPlayer();
        }

        if (gameObject.CompareTag("FieldOfViewEnemA") && controllerEnemA.status=="patrolling"&&controllerEnemA.bossFight==false)
        {
            controllerEnemA.detectPlayer();
        }

        if (gameObject.CompareTag("Detonator") && controllerEnemA.goingToExplode ==false)
        {
            controllerEnemA.detectPlayer();
            controllerEnemA.goingToExplode = true;
        }
    }

}
