﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayerEnemA : MonoBehaviour
{
    public GameObject player;
    public NavMeshAgent enemyAgent;
    public ControllerEnemA controllerEnemA;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        checkForStatus();
    }

    void checkForStatus()
    {
        if (controllerEnemA.status == "following" && controllerEnemA.isActive == true && player.GetComponent<PlayerLife>().death == false)
        {
            followPlayer();
        }
        else if (player.GetComponent<PlayerLife>().death == true)
        {
            stopMovement();
        }
    }

    public void followPlayer()
    {
        enemyAgent.SetDestination(player.transform.position);
    }

    public void stopFollowing()
    {
        enemyAgent.isStopped = true;
    }

    public void stopMovement()
    {
        enemyAgent.isStopped = true;
    }
}
