﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEnemA : MonoBehaviour
{
    public int initialHealth = 50;
    public int currentHealth = 50;
    public HealthBar healthBar;

    public bool playerDetected = false;
    public bool bossFight = false;

    public string status = "patrolling";
    public bool goingToExplode = false;

    public bool isActive = true;

    public bool tutorial = false;

    public bool canvasShowing = false;

    public AnimControllerEnemA animControllerEnemA;
    public PatrollingEnemA patrollingEnemA;
    public AnimationsEnemA animationsEnemA;
    public GameObject mainController;

    public GameObject canvasBar;

    void Start()
    {
        mainController = GameObject.Find("MainController");
        addEnemyToCounter();
        healthBar.SetMaxHealth(initialHealth);
        if (tutorial == false)
        {
            StartCoroutine(waitToActivate());

        }
    }

    void addEnemyToCounter()
    {
        mainController.GetComponent<MainController>().enemiesInScene++;
    }

    public void substractEnemyToCounter()
    {
        mainController.GetComponent<MainController>().enemiesInScene--;
    }

    void Update()
    {
        if (currentHealth < initialHealth && canvasShowing == false)
        {
            ShowHealthBar();
            canvasShowing = true;
        }
    }

    public void detectPlayer()
    {
        if (playerDetected==false)
        {
            playerDetected = true;
            animControllerEnemA.playerDetectedTrue();
            status = "following";
            FindObjectOfType<AudioManager>().Play("KamikazeDetect");
        }
    }
    
    IEnumerator waitToActivate()
    {
        yield return new WaitForSecondsRealtime(3);
        ActivateEnemy();
        //animationsEnemA.turnOnLights();
        
    }

    void ActivateEnemy()
    {
        if (bossFight==false)
        {
            patrollingEnemA.goToNextPoint();
        }
        else if (bossFight == true)
        {
            detectPlayer();
        }
    }
    void ShowHealthBar()
    {
        canvasBar.SetActive(true);
    }
}
