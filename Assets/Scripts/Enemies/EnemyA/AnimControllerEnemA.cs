﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerEnemA : MonoBehaviour
{
    public Animator anim;

    void Start()
    {

    }

    void Update()
    {

    }

    public void playerDetectedTrue()
    {
        anim.SetBool("playerDetected", true);
    }

    public void defeatedTrue()
    {
        anim.SetBool("Defeated", true);
    }
}
