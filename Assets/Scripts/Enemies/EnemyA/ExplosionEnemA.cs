﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ExplosionEnemA : MonoBehaviour
{
    public ControllerEnemA controllerEnemA;

    public AnimationsEnemA animationsEnemA;

    public GameObject explosionCol;

    public GameObject enemyGO;
    public GameObject model;
    public CapsuleCollider bodyCol;
    public GameObject canvasBar;

    public ParticleSystem explosionParticles;

    bool didntExplode = true;

    void Update()
    {
        checkForStatus();
    }

    void checkForStatus()
    {
        if (controllerEnemA.goingToExplode==true && didntExplode==true)
        {
            didntExplode = false;
            animationsEnemA.increaseLights();
            GetComponent<NavMeshAgent>().speed = 3;
            StartCoroutine(waitToExplode());
            StartCoroutine(waitToParticles());
        }
    }

    IEnumerator waitToParticles()
    {
        yield return new WaitForSeconds(1.4f);
        explosionParticles.Play();
    }

    IEnumerator waitToExplode()
    {
        yield return new WaitForSeconds(1.5f);
        Explode();
    }

    IEnumerator waitToDisableCol()
    {
        yield return new WaitForSeconds(0.1f);
        explosionCol.SetActive(false);
    }

    void Explode()
    {
        explosionCol.SetActive(true);
        model.SetActive(false);
        StartCoroutine(waitToDestroy());
        StartCoroutine(waitToDisableCol());
        bodyCol.enabled = false;
        canvasBar.SetActive(false);
        FindObjectOfType<AudioManager>().Play("PlayerExplosion");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            stopMovement();
        }
    }

    void stopMovement()
    {
        GetComponent<NavMeshAgent>().speed = 0;
    }

    IEnumerator waitToDestroy()
    {
        yield return new WaitForSeconds(1);
        controllerEnemA.substractEnemyToCounter();
        Destroy(enemyGO);
    }
}
