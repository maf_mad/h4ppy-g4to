﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationsEnemA : MonoBehaviour
{
    Color defaultColor;
    Color redTinted;
    public GameObject bodyGO;
    public GameObject model;
    public Light pointLight;
    public Light spotLight;
    public MeshRenderer shinyPart;
    public Transform bodyCol;
    public Transform particlesDefeatSP;

    /*public float shinyPartEmission;
    Material shinyPartMat;
    public Color shinyPartDefaultColor;*/

    public string currentColor = "default";
    public float lightIntensity = 6;

    public Vector3 initialPosition;

    void Start()
    {
        //Shader.EnableKeyword("_EmissionColor");

        GetDefaultColor();
        GetRedTintedColor();

        bodyGO.GetComponent<Renderer>().material.color = defaultColor;

        StartCoroutine(getInitialPosition());
        StartCoroutine(waitToAppear());
        StartCoroutine(waitToTurnLightsOn());
    }

    void Update()
    {
        particlesDefeatSP.position = bodyCol.position;
    }

    void GetRedTintedColor()
    {
        redTinted = bodyGO.GetComponent<Renderer>().material.color;
        redTinted.r = 255;
        redTinted.g = 0;
        redTinted.b = 0;
    }

    void GetDefaultColor()
    {
        defaultColor = bodyGO.GetComponent<Renderer>().material.color;
    }

    void changeColor()
    {
        if(currentColor=="default")
        {
            bodyGO.GetComponent<Renderer>().material.color = redTinted;
            currentColor = "redTinted";
        }
        else if (currentColor == "redTinted")
        {
            bodyGO.GetComponent<Renderer>().material.color = defaultColor;
            currentColor = "default";
        }
    }

    void turnOnOffLights()
    {
        if (lightIntensity == 6)
        {
            lightIntensity = 0;
            pointLight.intensity = lightIntensity;
            spotLight.intensity = lightIntensity;
        }
        else if (lightIntensity == 0)
        {
            lightIntensity = 6;
            pointLight.intensity = lightIntensity;
            spotLight.intensity = lightIntensity;
        }
    }

    IEnumerator startBlinkSequence()
    {
        yield return new WaitForSeconds(0.1f);
        changeColor();
        StartCoroutine(startBlinkSequence());
    }

    public void Blink()
    {
        InvokeRepeating("turnOnOffLights", 0 , 0.1f);

    }

    public void increaseLights()
    {
        pointLight.DOColor(Color.red*5,1.5f);
        shinyPart.material.DOColor(Color.red * 5, "_EmissionColor", 1.5f);
    }

    IEnumerator getInitialPosition()
    {
        yield return new WaitForSeconds(0.1f);
        initialPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    IEnumerator waitToAppear()
    {
        yield return new WaitForSeconds(0.2f);
        Appear();
    }

    IEnumerator waitToTurnLightsOn()
    {
        yield return new WaitForSeconds(2);
        turnLightsOn();
    }

    public void Appear()
    {
        model.GetComponent<Transform>().DOMoveY(0.5f, 1.5f).SetEase(Ease.Flash);
    }

    public void turnLightsOn()
    {
        pointLight.DOIntensity(6, 0.2f).SetEase(Ease.Flash);
        spotLight.DOIntensity(6, 0.2f).SetEase(Ease.Flash);
    }

    public void turnLightsOff()
    {
        pointLight.DOIntensity(0, 0.2f).SetEase(Ease.Flash);
        spotLight.DOIntensity(0, 0.2f).SetEase(Ease.Flash);
        shinyPart.material.DOColor(Color.red * 0, "_EmissionColor", 0.5f);
    }


    /*public void setIntensity()
    {
        shinyPart.GetComponent<Renderer>().material.SetColor("_EmissionColor",new Color(191,0,0,1));
    }*/
}
