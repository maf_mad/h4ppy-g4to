﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageEnemA : MonoBehaviour
{
    public ControllerEnemA controllerEnemA;
    public AnimControllerEnemA animControllerEnemA;
    public AnimationsEnemA animationEnemA;
    public FollowPlayerEnemA followPlayerEnemA;

    public GameObject enemyGO;
    public GameObject model;
    public GameObject canvasBar;

    public ParticleSystem particlesDefeatSP;
    public CapsuleCollider bodyCol;

    public int damageAmountByProjectile;

    public int damageAmountByBlade;

    public bool dead = false;

    void Update()
    {
        destroyIfDead();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerProjectile"))
        {
            takeDamagebyProjectile();
            Destroy(other.gameObject);
            controllerEnemA.detectPlayer();
        }
        if (other.gameObject.CompareTag("BladeHitbox"))
        {
            takeDamageByBlade();
            controllerEnemA.detectPlayer();
        }
        if (other.gameObject.CompareTag("SpecialHitbox"))
        {
            particlesDefeatSP.Play();
            model.SetActive(false);
            StartCoroutine(waitToDestroy());
            bodyCol.enabled = false;
            canvasBar.SetActive(false);
        }
    }

    void takeDamagebyProjectile()
    {
        controllerEnemA.currentHealth -= damageAmountByProjectile;
        controllerEnemA.healthBar.SetHealth(controllerEnemA.currentHealth);
    }

    void takeDamageByBlade()
    {
        controllerEnemA.currentHealth -= damageAmountByBlade;
        controllerEnemA.healthBar.SetHealth(controllerEnemA.currentHealth);
    }

    void destroyIfDead()
    {
        if (controllerEnemA.currentHealth <= 0 && dead == false)
        {
            dead = true;
            controllerEnemA.isActive = false;
            StartCoroutine(waitToDestroy());
            StartCoroutine(waitToStop());
            animationEnemA.turnLightsOff();
            animControllerEnemA.defeatedTrue();
            canvasBar.SetActive(false);
            FindObjectOfType<AudioManager>().Play("KamikazeDeath");
        }
    }

    IEnumerator waitToStop()
    {
        yield return new WaitForSeconds(0.5f);
        followPlayerEnemA.stopFollowing();
    }
    IEnumerator waitToDestroy()
    {
        yield return new WaitForSeconds(1);
        controllerEnemA.substractEnemyToCounter();
        Destroy(enemyGO);
    }
}
