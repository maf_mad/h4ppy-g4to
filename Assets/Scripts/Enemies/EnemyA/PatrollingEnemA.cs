﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrollingEnemA : MonoBehaviour
{
    public Transform point1;
    public Transform point2;
    public Transform point3;
    public Transform point4;
    public Transform nextPoint;

    public ControllerEnemA controllerEnemA;

    public NavMeshAgent enemyAgent;


    void Start()
    {
        nextPoint = point1;
    }


    public void goToNextPoint()
    {        
            enemyAgent.SetDestination(nextPoint.position);       
    }

    void OnTriggerEnter(Collider other)
    {
        if (controllerEnemA.status == "patrolling" && other.gameObject == nextPoint.gameObject)
        {
            setNextPoint();
            goToNextPoint();
        }
        
    }

    void setNextPoint()
    {
        if (nextPoint==point1)
        {
            nextPoint = point2;
        }
        else if (nextPoint == point2)
        {
            nextPoint = point3;
        }
        else if (nextPoint == point3)
        {
            nextPoint = point4;
        }
        else if (nextPoint == point4)
        {
            nextPoint = point1;
        }
    }

}
