﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ProjectileEnemB : MonoBehaviour
{
    float speed = 10;
    public Rigidbody projectileRigidbody;
    public GameObject collisionParticles;

    void Start()
    {
        grow();
        projectileRigidbody.velocity = transform.forward * speed;
        StartCoroutine(destroyProjectile());
    }

    IEnumerator destroyProjectile()
    {
        yield return new WaitForSeconds(10);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Instantiate(collisionParticles, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), gameObject.transform.rotation);

            Destroy(gameObject);
        }
        else if (other.CompareTag("Player"))
        {
            Instantiate(collisionParticles, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), gameObject.transform.rotation);
        }
    }

    void grow()
    {
        GetComponent<Transform>().DOScale(0.6f, 0.3f).SetEase(Ease.OutCubic);
    }
}
