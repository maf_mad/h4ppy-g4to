﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemB : MonoBehaviour
{

    public GameObject projectilePrefab;
    public GameObject projectileSpawnPoint;
    public GameObject player;
    public GameObject shootingParticlesPrefab;

    public float rechargeTime;
    public bool isCharged = true;

    public Vector3 initialPosition;

    public ControllerEnemB controllerEnemB;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        checkForStatus();
    }

    void checkForStatus()
    {
        if (controllerEnemB.status == "following" && controllerEnemB.canShoot == true && isCharged == true && controllerEnemB.isActive == true && controllerEnemB.isActive == true && player.GetComponent<PlayerLife>().death == false)
        {
            Fire();
        }
    }

    void Fire()
    {
        Instantiate(projectilePrefab, new Vector3(projectileSpawnPoint.transform.position.x, projectileSpawnPoint.transform.position.y, projectileSpawnPoint.transform.position.z), projectileSpawnPoint.transform.rotation);
        Instantiate(shootingParticlesPrefab, new Vector3(projectileSpawnPoint.transform.position.x, projectileSpawnPoint.transform.position.y, projectileSpawnPoint.transform.position.z), projectileSpawnPoint.transform.rotation);
        FindObjectOfType<AudioManager>().Play("TankShoot");
        isCharged = false;
        StartCoroutine(Recharge());
    }

    IEnumerator Recharge()
    {
        yield return new WaitForSeconds(rechargeTime);
        isCharged = true;
    }

    
}
