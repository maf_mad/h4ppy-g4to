﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageEnemB : MonoBehaviour
{
    public ControllerEnemB controllerEnemB;
    public AnimationsEnemB animationEnemB;
    public AnimControllerEnemB animControllerEnemB;

    public GameObject enemyGO;
    public ParticleSystem particlesDefeatSP;
    public GameObject model;
    public CapsuleCollider bodyCol;
    public GameObject canvasBar;

    public int damageAmount;
    public bool dead = false;

    void Update()
    {
        destroyIfDead();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BladeHitbox"))
        {
            takeDamage();
            controllerEnemB.detectPlayer();
        }

        if (other.gameObject.CompareTag("SpecialHitbox"))
        {
            particlesDefeatSP.Play();
            model.SetActive(false);
            StartCoroutine(waitToDestroy());
            bodyCol.enabled = false;
            canvasBar.SetActive(false);
            controllerEnemB.isActive = false;
        }
    }

    void takeDamage()
    {
        controllerEnemB.currentHealth -= damageAmount;
        controllerEnemB.healthBar.SetHealth(controllerEnemB.currentHealth);
    }

    void destroyIfDead()
    {
        if (controllerEnemB.currentHealth <= 0 && dead == false)
        {
            dead = true;
            animationEnemB.canRotate = false;
            controllerEnemB.isActive = false;
            animationEnemB.turnOffLights();
            StartCoroutine(waitToDestroy());
            animControllerEnemB.deathAnim();
            canvasBar.SetActive(false);
            FindObjectOfType<AudioManager>().Play("TankDeath");
        }
    }

    IEnumerator waitToDestroy()
    {
        yield return new WaitForSeconds(1.5f);
        controllerEnemB.substractEnemyToCounter();
        destroyGO();
    }

    void destroyGO()
    {
        Destroy(enemyGO);
    }
}
