﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationsEnemB : MonoBehaviour
{
    public GameObject baseGO;
    public GameObject headGO;
    public GameObject playerGO;
    public GameObject model;
    public GameObject pointLightMiddle;
    public GameObject pointLightBase;
    public MeshRenderer middle;
    public Transform particlesDefeatSP;

    public bool canRotate = false;

    public ControllerEnemB controllerEnemB;

    void Start()
    {
        playerGO = GameObject.Find("Player");
        StartCoroutine(waitToAppear());
        StartCoroutine(waitToTurnLightsOn());
    }

    void Update()
    {
        baseRotation();
        headRotation();
        particlesDefeatSP.position = model.transform.position;
    }

    void baseRotation()
    {
        if (controllerEnemB.isActive==true)
        {
            baseGO.transform.Rotate(Vector3.up * 150 * Time.deltaTime, Space.World);
        }
    }

    void headRotation()
    {
        if (canRotate)
        {
            headGO.transform.LookAt(new Vector3(playerGO.transform.position.x, headGO.transform.position.y, playerGO.transform.position.z));
        }
    }

    //------

    IEnumerator waitToAppear()
    {
        yield return new WaitForSeconds(0.2f);
        Appear();
    }
    IEnumerator waitToTurnLightsOn()
    {
        yield return new WaitForSeconds(2);
        turnLightsOn();
    }

    IEnumerator waitToRotate()
    {
        yield return new WaitForSeconds(1);
        canRotate = true;
    }

    public void activateRotation()
    {
        StartCoroutine(waitToRotate());
    }

    public void Appear()
    {
        model.GetComponent<Transform>().DOMoveY(0, 1.5f).SetEase(Ease.Flash);
    }

    public void turnLightsOn()
    {
        pointLightMiddle.GetComponent<Light>().DOIntensity(6, 0.2f).SetEase(Ease.Flash);
        pointLightBase.GetComponent<Light>().DOIntensity(5, 0.2f).SetEase(Ease.Flash);
    }

    public void turnOffLights()
    {
        pointLightMiddle.GetComponent<Light>().DOIntensity(0, 0.2f).SetEase(Ease.Flash);
        pointLightBase.GetComponent<Light>().DOIntensity(0, 0.2f).SetEase(Ease.Flash);
        middle.material.DOColor(Color.red * 0, "_EmissionColor", 1);
    }
}
