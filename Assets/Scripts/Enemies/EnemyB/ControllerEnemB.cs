﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEnemB : MonoBehaviour
{
    public int initialHealth = 60;
    public int currentHealth = 60;
    public HealthBar healthBar;

    public string status = "patrolling";
    public bool isActive = true;
    public bool bossFight = false;
    public bool canShoot = false;
    public bool canvasShowing = false;

    public AnimControllerEnemB animControllerEnemB;
    public AnimationsEnemB animationsEnemB;

    public GameObject canvasBar;
    public GameObject mainController;

    void Start()
    {
        mainController = GameObject.Find("MainController");
        addEnemyToCounter();

        healthBar.SetMaxHealth(initialHealth);

        if (bossFight == true)
        {
            detectPlayer();
        }
    }

    void addEnemyToCounter()
    {
        mainController.GetComponent<MainController>().enemiesInScene++;
    }

    public void substractEnemyToCounter()
    {
        mainController.GetComponent<MainController>().enemiesInScene--;
    }

    void Update()
    {
        if(currentHealth < initialHealth && canvasShowing == false)
        {
            ShowHealthBar();
            canvasShowing = true;
        }
    }

    public void detectPlayer()
    {
        animControllerEnemB.detectionAnim();
        StartCoroutine(waitForAnimation());
        StartCoroutine(waitToShot());
        FindObjectOfType<AudioManager>().Play("TankDetect");
    }

    IEnumerator waitForAnimation()
    {
        yield return new WaitForSeconds(3);
        animationsEnemB.activateRotation();
        status = "following";
    }
    IEnumerator waitToShot()
    {
        yield return new WaitForSeconds(4);
        canShoot = true;
    }

    void ShowHealthBar()
    {
        canvasBar.SetActive(true);
    }
}
