﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerEnemB : MonoBehaviour
{
    public Animator anim;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void detectionAnim()
    {
        anim.SetBool("PlayerDetected", true);
    }

    public void deathAnim()
    {
        anim.SetBool("IsDead", true);
    }
}
