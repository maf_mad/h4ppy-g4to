﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StopAndContinueNavEnemB : MonoBehaviour
{
    public GameObject enemyGO;
    public float initialSpeed;

    void Start()
    {
        initialSpeed = enemyGO.GetComponent<NavMeshAgent>().speed;
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemyGO.GetComponent<NavMeshAgent>().speed = 0;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemyGO.GetComponent<NavMeshAgent>().speed = initialSpeed;
        }
    }
}
