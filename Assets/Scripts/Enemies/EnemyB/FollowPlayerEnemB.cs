﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayerEnemB : MonoBehaviour
{
    public GameObject player;
    public NavMeshAgent enemBAgent;
    public ControllerEnemB controllerEnemB;

    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        checkForStatus();
    }

    void checkForStatus()
    {
        if (controllerEnemB.status == "following" && controllerEnemB.isActive ==true && player.GetComponent<PlayerLife>().death == false)
        {
            followPlayer();
        }
        else if (player.GetComponent<PlayerLife>().death == true)
        {
            stopMovement();
        }
        
    }

    public void followPlayer()
    {
        enemBAgent.SetDestination(player.transform.position);

    }

    public void stopMovement()
    {
        enemBAgent.isStopped = true;
    }
}
