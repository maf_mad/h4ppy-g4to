﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Level1Controller : MonoBehaviour
{
    public NavMeshAgent playerAgent;
    public MainController mainController;
    public PlayerController playerController;
    public GameObject nextSceneFadeOff;
    public bool missionFinished = false;
    public bool checking = false;

    void Start()
    {
        StartCoroutine(waitToCheckingForEnemies());
    }

    void Update()
    {
        if (mainController.enemiesInScene <= 0 && missionFinished == false && checking)
        {
            missionFinished = true;
            playerController.disableControls();
            playerAgent.SetDestination(transform.position);
            StartCoroutine(waitToChangeScene());
        }
    }

    IEnumerator waitToCheckingForEnemies()
    {
        yield return new WaitForSeconds(1);
        checking = true; ;
    }

    IEnumerator waitToChangeScene()
    {
        yield return new WaitForSeconds(1);
        nextSceneFadeOff.SetActive(true);
    }
}
