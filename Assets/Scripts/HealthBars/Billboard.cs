﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Transform target;

    private void Start()
    {
        target = Camera.main.transform;
    }


    private void LateUpdate()
    {
        transform.LookAt(transform.position + target.rotation * -Vector3.back, target.rotation * -Vector3.down);
    }
}
