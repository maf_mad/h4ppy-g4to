﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{
    Animator doorAnim;

    void Start()
    {
        doorAnim = GetComponent<Animator>();
    }

    public void SlideDoors(bool state)
    {
        doorAnim.SetBool("slide", state);
    }
}
