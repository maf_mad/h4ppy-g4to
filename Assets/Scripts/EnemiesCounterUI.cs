﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesCounterUI : MonoBehaviour
{
    public Text enemiesCounter;
    public MainController mainController;

    void Start()
    {
        
    }

    void Update()
    {
        enemiesCounter.text = mainController.enemiesInScene.ToString();
    }
}
