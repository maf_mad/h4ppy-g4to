﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using DG.Tweening;

public class TutorialSequence : MonoBehaviour
{
    public Text tutorialText;
    Color transparent = Color.white;
    int transparency = 0;
    public int phase = 1;
    public Transform tutorialPoint;
    public Transform exitPoint;
    public BoxCollider activatorTrigger;
    public PlayerController playerController;
    public NavMeshAgent playerAgent;

    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;

    public GameObject exitPointObj;
    public GameObject doorTrigger;

    public GameObject nextSceneFadeOff;
    public bool gameStarted = false;


    void Start()
    {
        transparent.a = 0;
        tutorialText.color = transparent;
        InvokeRepeating("fadeText",0,0.5f);
        tutorialText.text = "Use Right click to move.";
        StartCoroutine(waitForLoadingScreen());
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1)&&phase==1 && gameStarted == true)
        {
            phase = 2;
            playerController.enableControls();
            hidetext();
        }

        if (Input.GetMouseButtonDown(1) && phase == 3)
        {
            phase = 4;
            playerController.enableControls();
            hidetext();
        }

        if (enemy1 == null && phase ==4)
        {
            phase = 5;
            playerAgent.SetDestination(tutorialPoint.position);
            playerController.disableControls();
            StartCoroutine(waitToActivateEnemy2());
            StartCoroutine(waitToShowText3());
            tutorialText.text = "Be careful, this enemy shoots, and it can only be damaged by your Blade, use it with Left Click.";
        }

        if (Input.GetMouseButtonDown(0) && phase == 6)
        {
            phase = 7;
            playerController.enableControls();
            hidetext();
        }

        if (enemy2 == null && phase == 7)
        {
            phase = 8;
            playerAgent.SetDestination(tutorialPoint.position);
            playerController.disableControls();
            StartCoroutine(waitToActivateEnemy3());
            StartCoroutine(waitToFillSpBar());
            StartCoroutine(waitToShowText4());
            tutorialText.text = "When your blue bar is full, you can use the Charged Tackle. \n To do it, use middle click or space bar. It lasts 5 seconds.";
        }

        if (Input.GetMouseButtonDown(2) || Input.GetKeyDown(KeyCode.Space) )
        {
            if (phase == 9)
            {
                phase = 10;
                playerController.enableControls();
                hidetext();
            }
            
        }

        if (enemy3 == null && phase == 10)
        {
            phase = 11;
            playerController.disableControls();
            playerController.stopSpecial();
            StartCoroutine(waitToShowText5());
            tutorialText.text = "Congratulations, you have completed the tutorial. Click to continue.";
            //exitPointObj.SetActive(true);
            doorTrigger.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0) && phase == 12)
        {
            phase = 13;
            playerAgent.SetDestination(exitPoint.position);
            hidetext();
            StartCoroutine(waitToChangeScene());
        }
    }

    void fadeText()
    {
        if (transparency == 0)
        {
            transparency = 1;
            tutorialText.DOFade(1, 0.5f).SetEase(Ease.OutCubic);
        }
        else if (transparency == 1)
        {
            transparency = 0;
            tutorialText.DOFade(0, 0.5f).SetEase(Ease.InCubic);
        }
    }

    void hidetext()
    {
        CancelInvoke("fadeText");
        tutorialText.DOFade(0, 0.5f).SetEase(Ease.InCubic);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerController.stopSpecial();
            playerController.disableControls();
            playerAgent.SetDestination(tutorialPoint.position);
            StartCoroutine(waitToActivateEnemy1());
            StartCoroutine(waitToShowText2());
            tutorialText.text = "Right click on this enemy to use your laser gun. Be careful, if it get close, it will explode.";
            activatorTrigger.enabled = false;
        }
    }

    IEnumerator waitToActivateEnemy1()
    {
        yield return new WaitForSeconds(2f);
        enemy1.SetActive(true);
    }

    IEnumerator waitToShowText2()
    {
        yield return new WaitForSeconds(3f);
        phase = 3;
        InvokeRepeating("fadeText",0,0.5f);
    }

    IEnumerator waitToActivateEnemy2()
    {
        yield return new WaitForSeconds(3f);
        enemy2.SetActive(true);
    }

    IEnumerator waitToShowText3()
    {
        yield return new WaitForSeconds(4f);
        phase = 6;
        InvokeRepeating("fadeText", 0, 0.5f);
    }

    IEnumerator waitToActivateEnemy3()
    {
        yield return new WaitForSeconds(3f);
        enemy3.SetActive(true);
    }

    IEnumerator waitToFillSpBar()
    {
        yield return new WaitForSeconds(3f);
        playerController.currentSp=playerController.maxSp;
    }

    IEnumerator waitToShowText4()
    {
        yield return new WaitForSeconds(4f);
        phase = 9;
        InvokeRepeating("fadeText", 0, 0.5f);
    }

    IEnumerator waitToShowText5()
    {
        yield return new WaitForSeconds(1f);
        phase = 12;
        InvokeRepeating("fadeText", 0, 0.5f);
    }

    IEnumerator waitToChangeScene()
    {
        yield return new WaitForSeconds(2);
        nextSceneFadeOff.SetActive(true);
    }

    IEnumerator waitForLoadingScreen()
    {
        yield return new WaitForSeconds(4f);
        gameStarted = true;
    }
}
