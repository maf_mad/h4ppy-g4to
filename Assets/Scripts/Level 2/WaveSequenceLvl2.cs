﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class WaveSequenceLvl2 : MonoBehaviour
{
    public Transform[] spawnPointsEnem1;
    public Transform[] spawnPointsEnem2;

    public GameObject enemy1;
    public GameObject enemy2;

    public MainController mainController;
    public PlayerController playerController;

    public NavMeshAgent playerAgent;
    public Transform exitPoint;
    public GameObject nextSceneFadeOff;

    public bool sequenceFinished = false;
    public bool levelCompleted = false;

    void Start()
    {
        
    }

    void Update()
    {
        checkForEnemies();
    }

    public void startSequence()
    {
        InvokeRepeating("spawnEnem1", 0, 3.5f);
        InvokeRepeating("spawnEnem2", 0, 8f);
        StartCoroutine(waitToStopSpawning());
    }

    void checkForEnemies()
    {
        if (mainController.enemiesInScene <= 0 && sequenceFinished == true && levelCompleted == false)
        {
            levelCompleted = true;
            exitSequence();
        }
    }

    void spawnEnem1()
    {
        for (int i = 0; i < spawnPointsEnem1.Length; i++)
        {
            Instantiate(enemy1, spawnPointsEnem1[i].position, spawnPointsEnem1[i].rotation);
        }
    }

    void spawnEnem2()
    {
        for (int i = 0; i < spawnPointsEnem2.Length; i++)
        {
            Instantiate(enemy2, spawnPointsEnem2[i].position, spawnPointsEnem2[i].rotation);
        }
    }

    IEnumerator waitToStopSpawning()
    {
        yield return new WaitForSeconds(40);
        CancelInvoke("spawnEnem1");
        CancelInvoke("spawnEnem2");
        sequenceFinished = true;
    }

    void exitSequence()
    {
        playerController.disableControls();
        playerController.stopSpecial();
        playerAgent.SetDestination(exitPoint.position);
        StartCoroutine(waitToChangeScene());
    }

    IEnumerator waitToChangeScene()
    {
        yield return new WaitForSeconds(2);
        nextSceneFadeOff.SetActive(true);
    }
}
