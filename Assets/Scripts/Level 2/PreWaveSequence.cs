﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PreWaveSequence : MonoBehaviour
{
    public Transform mainCamera;
    public CameraController mainCameraController;
    public Transform battlePoint;
    public NavMeshAgent playerAgent;
    public PlayerController playerController;
    public WaveSequenceLvl2 waveSequence;
    public MissionUI missionUI;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerController.stopSpecial();
            mainCameraController.inCutscene = true;
            playerController.disableControls();
            movePlayer();
            StartCoroutine(waitToMoveCamera());
            StartCoroutine(waitToStartSequence());
            StartCoroutine(waitToShowMission());
        }
    }

    void movePlayer()
    {
        playerAgent.SetDestination(battlePoint.position);
    }

    IEnumerator waitToMoveCamera()
    {
        yield return new WaitForSeconds(1.5f);
        moveCamera();
    }

    IEnumerator waitToShowMission()
    {
        yield return new WaitForSeconds(2.5f);
        missionUI.showAndHideMission();
    }

    void moveCamera()
    {
        mainCamera.DOMove(new Vector3(0, 25.9f, 41.8f), 1);
        mainCamera.DORotate(new Vector3(50, 0, 0), 1);
    }

    IEnumerator waitToStartSequence()
    {
        yield return new WaitForSeconds(3.5f);
        waveSequence.startSequence();
        playerController.enableControls();
    }
}
